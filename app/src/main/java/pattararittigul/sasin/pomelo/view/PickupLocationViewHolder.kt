package pattararittigul.sasin.pomelo.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_card.view.*
import pattararittigul.sasin.pomelo.model.PickupLocationCardModel

class PickupLocationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setupView(pickup: PickupLocationCardModel.PickUpModel) {
        itemView.cityText.text = pickup.city
        itemView.shopText.text = pickup.alias
        itemView.aliasText.text = pickup.address
    }

}
