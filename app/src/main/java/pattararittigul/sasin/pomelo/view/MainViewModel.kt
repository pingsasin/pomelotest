package pattararittigul.sasin.pomelo.view

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.NonCancellable.isActive
import pattararittigul.sasin.pomelo.extension.executePickupLocations
import pattararittigul.sasin.pomelo.mapper.mapToCardModels
import pattararittigul.sasin.pomelo.model.PickupLocationCardModel

class MainViewModel : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val pickupLocationData = MutableLiveData<MutableList<PickupLocationCardModel.PickUpModel>>()

    fun pickupLocation() {
        executePickupLocations()
            .subscribeBy(
                onNext = {
                    pickupLocationData.postValue(filterActive(it.pickup.mapToCardModels()))
                },
                onError = {}
            )
            .addTo(compositeDisposable)
    }

    fun pickupLocationResult(): LiveData<MutableList<PickupLocationCardModel.PickUpModel>> =
        pickupLocationData


    fun sortingWithCurrentLocation(currentLocation: LatLng) {
        pickupLocationData.value?.sortedBy {
            findDistanceBetween(currentLocation, it.latLng)
        }?.toMutableList()
            .also {
            pickupLocationData.postValue(it)
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun filterActive(list: MutableList<PickupLocationCardModel.PickUpModel>): MutableList<PickupLocationCardModel.PickUpModel> =
       list.filter { it.isActive }.toMutableList()


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun findDistanceBetween(
        currentLocation: LatLng,
        pickupLocation: LatLng
    ) = SphericalUtil.computeDistanceBetween(currentLocation, pickupLocation)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}