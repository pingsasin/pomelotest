package pattararittigul.sasin.pomelo.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pattararittigul.sasin.pomelo.R
import pattararittigul.sasin.pomelo.model.PickupLocationCardModel

class PickupLocationAdapter(private val pickupLocationList: MutableList<PickupLocationCardModel.PickUpModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return PickupLocationViewHolder(view)
    }

    override fun getItemCount(): Int = pickupLocationList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PickupLocationViewHolder) {
            val pickup = pickupLocationList[position]
            holder.setupView(pickup)
        }

    }

    fun updateList(pickupLocations: MutableList<PickupLocationCardModel.PickUpModel>) {
        pickupLocationList.clear()
        pickupLocationList.addAll(pickupLocations)
        notifyDataSetChanged()
    }

}
