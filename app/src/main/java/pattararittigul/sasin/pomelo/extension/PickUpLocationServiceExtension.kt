package pattararittigul.sasin.pomelo.extension

import pattararittigul.sasin.pomelo.api.PickupAPI
import pattararittigul.sasin.pomelo.service.RetrofitService

private fun pickupService() = RetrofitService.retrofit().create(PickupAPI::class.java)

fun executePickupLocations() = pickupService().getPickupLocation().execute()