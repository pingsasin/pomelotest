package pattararittigul.sasin.pomelo.api

import io.reactivex.Observable
import pattararittigul.sasin.pomelo.model.PickupLocationRemoteModel
import retrofit2.http.GET

interface PickupAPI {
    @GET("v3/pickup-locations")
    fun getPickupLocation(): Observable<PickupLocationRemoteModel>
}