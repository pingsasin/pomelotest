package pattararittigul.sasin.pomelo.mapper

import com.google.android.gms.maps.model.LatLng
import pattararittigul.sasin.pomelo.model.PickupLocationCardModel
import pattararittigul.sasin.pomelo.model.PickupLocationRemoteModel

fun List<PickupLocationRemoteModel.Pickup>.mapToCardModels(): MutableList<PickupLocationCardModel.PickUpModel> {
    return this.map { it.mapToCardModel() }.toMutableList()
}

fun PickupLocationRemoteModel.Pickup.mapToCardModel(): PickupLocationCardModel.PickUpModel {
    return PickupLocationCardModel.PickUpModel(
        address = address1.defaultWithNoData(),
        city = city.defaultWithNoData(),
        alias = alias.defaultWithNoData(),
        latLng = defaultLatLngIfNeed(latitude,longitude),
        isActive = active?:false
    )
}

private fun String?.defaultWithNoData(): String = if (this.isNullOrBlank()) "(no data)" else this

private fun defaultLatLngIfNeed(latitude: Double?, longitude: Double?): LatLng =
    if (latitude == null || longitude == null) LatLng(0.0,0.0) else LatLng(latitude,longitude)