package pattararittigul.sasin.pomelo.model


import com.google.gson.annotations.SerializedName

data class PickupLocationRemoteModel(
    @SerializedName("number_of_new_locations")
    var numberOfNewLocations: Int? = null, // 5
    @SerializedName("pickup")
    var pickup: List<Pickup> = listOf()
) {
    data class Pickup(
        @SerializedName("feature")
        var feature: String? = null, // fast_delivery
        @SerializedName("id_pickup_location")
        var idPickupLocation: Int? = null, // 39
        @SerializedName("id_country")
        var idCountry: Int? = null, // 206
        @SerializedName("id_state")
        var idState: Int? = null, // 315
        @SerializedName("id_carrier")
        var idCarrier: Int? = null, // 35
        @SerializedName("company")
        var company: Any? = null, // null
        @SerializedName("nps_link")
        var npsLink: String? = null, // https://pomelo3.typeform.com/to/EDFJ5z
        @SerializedName("alias")
        var alias: String? = null,
        @SerializedName("address1")
        var address1: String? = null,
        @SerializedName("address2")
        var address2: String? = null,
        @SerializedName("district")
        var district: String? = null,
        @SerializedName("city")
        var city: String? = null,
        @SerializedName("postcode")
        var postcode: String? = null, // 10110
        @SerializedName("latitude")
        var latitude: Double? = null, // 13.7314
        @SerializedName("longitude")
        var longitude: Double? = null, // 100.569212
        @SerializedName("phone")
        var phone: String? = null, // 02-003-6072
        @SerializedName("nearest_bts")
        var nearestBts: String? = null,
        @SerializedName("notable_area")
        var notableArea: Any? = null, // null
        @SerializedName("hours1")
        var hours1: String? = null,
        @SerializedName("hours2")
        var hours2: String? = null,
        @SerializedName("hours3")
        var hours3: String? = null,
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("is_featured")
        var isFeatured: Boolean? = null, // false
        @SerializedName("subtype")
        var subtype: String? = null, // store
        @SerializedName("store_image_path")
        var storeImagePath: String? = null, // {"primary":{"landscape":"img/pickup-locations/main-image-landscape/emquartier.jpg","full_landscape":"https://pomelofashion-staging.imgix.net/img/pickup-locations/main-image-landscape/emquartier.jpg","portrait":"img/retail/stores/default/primary/default_portrait.jpg","full_portrait":"https://pomelofashion-staging.imgix.net/img/retail/stores/default/primary/default_portrait.jpg"},"secondary":"img/pickup-locations/secondary-image/emquartier.jpg","full_secondary":"https://pomelofashion-staging.imgix.net/img/pickup-locations/secondary-image/emquartier.jpg"}
        @SerializedName("floormap_image_path")
        var floormapImagePath: String? = null, // {"main":"img/pickup-locations/floor-map/emquartier.jpg","full_main":"https://pomelofashion-staging.imgix.net/img/pickup-locations/floor-map/emquartier.jpg","zoomed":"img/pickup-locations/floor-map-zoomed/emquartier.jpg","full_zoomed":"https://pomelofashion-staging.imgix.net/img/pickup-locations/floor-map-zoomed/emquartier.jpg"}
        @SerializedName("active")
        var active: Boolean? = null, // true
        @SerializedName("floor_number")
        var floorNumber: String? = null, // 1
        @SerializedName("status")
        var status: String? = null, // active
        @SerializedName("id_zone")
        var idZone: Int? = null, // 3
        @SerializedName("features")
        var features: List<Feature?>? = null,
        @SerializedName("is_new_location")
        var isNewLocation: Boolean? = null, // false
        @SerializedName("type")
        var type: String? = null, // pickup
        @SerializedName("hours")
        var hours: List<String?>? = null,
        @SerializedName("images")
        var images: Images? = null,
        @SerializedName("is_default_location")
        var isDefaultLocation: Boolean? = null, // false
        @SerializedName("id_partner_store")
        var idPartnerStore: Int? = null, // 22
        @SerializedName("payment_methods")
        var paymentMethods: List<PaymentMethod?>? = null
    ) {
        data class Feature(
            @SerializedName("type")
            var type: String? = null, // fast_delivery
            @SerializedName("description")
            var description: String? = null // ShopOnline & Try Order On Offline
        )

        data class Images(
            @SerializedName("store")
            var store: Store? = null,
            @SerializedName("floormap")
            var floormap: Floormap? = null
        ) {
            data class Store(
                @SerializedName("primary")
                var primary: Primary? = null,
                @SerializedName("secondary")
                var secondary: String? = null, // img/pickup_location/images/20200304164123_5e5f77c317250_file.jpg
                @SerializedName("full_secondary")
                var fullSecondary: String? = null // img/pickup_location/images/20200304164123_5e5f77c317250_file.jpg
            ) {
                data class Primary(
                    @SerializedName("landscape")
                    var landscape: String? = null, // img/pickup_location/images/20200304164134_5e5f77ce3d623_file.jpg
                    @SerializedName("full_landscape")
                    var fullLandscape: String? = null, // img/pickup_location/images/20200304164134_5e5f77ce3d623_file.jpg
                    @SerializedName("portrait")
                    var portrait: String? = null, // img/retail/stores/default/primary/default_portrait.jpg
                    @SerializedName("full_portrait")
                    var fullPortrait: String? = null // https://pomelofashion-staging.imgix.net/img/retail/stores/default/primary/default_portrait.jpg
                )
            }

            data class Floormap(
                @SerializedName("main")
                var main: String? = null, // img/pickup_location/images/20200304164211_5e5f77f31bb6e_file.jpg
                @SerializedName("full_main")
                var fullMain: String? = null, // img/pickup_location/images/20200304164211_5e5f77f31bb6e_file.jpg
                @SerializedName("zoomed")
                var zoomed: String? = null, // img/pickup_location/images/20200304164148_5e5f77dc7f07c_file.jpg
                @SerializedName("full_zoomed")
                var fullZoomed: String? = null // img/pickup_location/images/20200304164148_5e5f77dc7f07c_file.jpg
            )
        }

        data class PaymentMethod(
            @SerializedName("id_partner_store")
            var idPartnerStore: Int? = null, // 22
            @SerializedName("id_payment_type")
            var idPaymentType: Int? = null, // 20
            @SerializedName("description")
            var description: String? = null, // PAYONLINE
            @SerializedName("active")
            var active: Int? = null, // 1
            @SerializedName("position")
            var position: Int? = null, // 1
            @SerializedName("is_new")
            var isNew: Boolean? = null // false
        )
    }
}