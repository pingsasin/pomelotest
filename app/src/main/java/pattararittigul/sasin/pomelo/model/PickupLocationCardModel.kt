package pattararittigul.sasin.pomelo.model

import com.google.android.gms.maps.model.LatLng

data class PickupLocationCardModel(
    var pickups: MutableList<PickUpModel> = mutableListOf()
) {
    data class PickUpModel(
        var address: String,
        var city: String,
        var alias: String,
        var latLng: LatLng,
        var isActive: Boolean
    )
}