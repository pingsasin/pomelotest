package pattararittigul.sasin.pomelo.service

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitService {
    companion object {
        fun retrofit(): Retrofit {
            val url = "https://45434c1b-1e22-4af2-8c9f-c2d99ffa4896.mock.pstmn.io/"
            val gSon = GsonBuilder()
                .setLenient()
                .create()
            return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .client(getOkHttpClient())
                .baseUrl(url)
                .build()
        }

        @JvmStatic
        private fun getOkHttpClient(): OkHttpClient {
            val bodyLog = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            val client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(bodyLog)
                .addInterceptor {
                    val request = (
                             it.request()
                                .newBuilder()
                                .addHeader("content-type", "application/json")
                                .addHeader("x-api-key", "PMAK-5f9f78c35dfcb3003bc3caba-0123b96555c97ae33319fbd572d4236a7e"))
                        .build()
                    it.proceed(request)
                }
            return client.build()
        }
    }
}