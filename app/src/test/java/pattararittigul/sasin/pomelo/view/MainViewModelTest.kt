package pattararittigul.sasin.pomelo.view

import com.google.android.gms.maps.model.LatLng
import org.junit.Assert
import org.junit.Test
import pattararittigul.sasin.pomelo.model.PickupLocationCardModel

class MainViewModelTest {

    @Test
    fun `find distance between locations case 1`() {
        val result1 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(2.0, 2.0))
        val result2 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(3.0, 2.0))
        Assert.assertTrue(result1 < result2)
    }

    @Test
    fun `find distance between locations case 2`() {
        val result1 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(2.0, 2.0))
        val result2 = MainViewModel().findDistanceBetween(LatLng(2.0, 2.0), LatLng(1.0, 1.0))
        Assert.assertTrue(result1 == result2)
    }

    @Test
    fun `find distance between locations case 3`() {
        val result1 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(2.0, 2.0))
        val result2 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(2.0, 2.0))
        Assert.assertTrue(result1 == result2)
    }

    @Test
    fun `find distance between locations case 4`() {
        val result1 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(10.0, 10.0))
        val result2 = MainViewModel().findDistanceBetween(LatLng(1.0, 1.0), LatLng(2.0, 2.0))
        Assert.assertTrue(result1 > result2)
    }

    @Test
    fun `filter is active case 1`() {
        val list = mutableListOf(
            PickupLocationCardModel.PickUpModel(
                address = "address1",
                city = "city1",
                alias = "alias1",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address2",
                city = "city2",
                alias = "alias2",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address3",
                city = "city3",
                alias = "alias3",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address4",
                city = "city4",
                alias = "alias4",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address5",
                city = "city5",
                alias = "alias5",
                latLng = LatLng(1.0,1.0),
                isActive = true
            )
        )

          val expectResult = mutableListOf(
            PickupLocationCardModel.PickUpModel(
                address = "address2",
                city = "city2",
                alias = "alias2",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address3",
                city = "city3",
                alias = "alias3",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address4",
                city = "city4",
                alias = "alias4",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address5",
                city = "city5",
                alias = "alias5",
                latLng = LatLng(1.0,1.0),
                isActive = true
            )
        )
        Assert.assertEquals(expectResult,MainViewModel().filterActive(list))
    }


    @Test
    fun `filter is active case 2`() {
        val list = mutableListOf(
            PickupLocationCardModel.PickUpModel(
                address = "address1",
                city = "city1",
                alias = "alias1",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address2",
                city = "city2",
                alias = "alias2",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address3",
                city = "city3",
                alias = "alias3",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address4",
                city = "city4",
                alias = "alias4",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address5",
                city = "city5",
                alias = "alias5",
                latLng = LatLng(1.0,1.0),
                isActive = true
            )
        )

        val expectResult = mutableListOf(
            PickupLocationCardModel.PickUpModel(
                address = "address2",
                city = "city2",
                alias = "alias2",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address4",
                city = "city4",
                alias = "alias4",
                latLng = LatLng(1.0,1.0),
                isActive = true
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address5",
                city = "city5",
                alias = "alias5",
                latLng = LatLng(1.0,1.0),
                isActive = true
            )
        )
        Assert.assertEquals(expectResult,MainViewModel().filterActive(list))
    }


    @Test
    fun `filter is active case 3`() {
        val list = mutableListOf(
            PickupLocationCardModel.PickUpModel(
                address = "address1",
                city = "city1",
                alias = "alias1",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address2",
                city = "city2",
                alias = "alias2",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address3",
                city = "city3",
                alias = "alias3",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address4",
                city = "city4",
                alias = "alias4",
                latLng = LatLng(1.0,1.0),
                isActive = false
            ),
            PickupLocationCardModel.PickUpModel(
                address = "address5",
                city = "city5",
                alias = "alias5",
                latLng = LatLng(1.0,1.0),
                isActive = false
            )
        )

        val expectResult = mutableListOf<PickupLocationCardModel.PickUpModel>()
        Assert.assertEquals(expectResult,MainViewModel().filterActive(list))
    }
}